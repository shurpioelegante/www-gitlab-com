---
layout: markdown_page
title: "Category Strategy - DevOps Score"
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the category strategy page for DevOps Score.

This strategy is a work in progress and everyone can contribute by sharing their feedback directly on [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1499), via e-mail, or Twitter.

### Overview

With [VSM](https://about.gitlab.com/direction/manage/value_stream_management/) and [Code Analytics](https://about.gitlab.com/direction/manage/code-analytics), GitLab is trying to improve organizations' understanding of how their people and codebase interact in order to improve the speed and quality of software delivery. More and more new users are signing up on GitLab.com and through that expanding community, we hope to be able to draw insights into how the usage of GitLab's different components correlates with and improves the efficiency, velocity and quality of an organization. We would also like to enable Executives to compare their organization with the rest of the community and identify areas for improvement.

Our first attempt at providing instances with a single metric for GitLab adoption and engagement was [ConvDev Index](https://docs.gitlab.com/ee/user/instance_statistics/convdev.html). As soon as the index was released, however, we recognized that it has opened an enormous space for research, which will evolve with our software.

### Where we are Headed

The goal of DevOps is to increase quality, velocity and performance and we are helping customers to improve across these dimenions with [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/). There are multiple levels of detail required to manage and improve bottlenecks, but it's also important to define clear top line metrics that can be comparable across industries and companies. We believe the 4 main DORA metrics for DevOps success are a great start to that, which we will complement with other measures that companies have found powerful in providing a holistic picture of the health of their software development. Some of these include:
- Deployment frequency - reducing the size and time between deployments makes it easier to test and release (staging, canary, production)
- Defect escape rate - % of bugs identified after software is released in production
- Deployment size - story points, issues delivered in each deployment
- Mean time to detection (MTTD) - great appplication monitoring can help reduce the time to detect failures, which in turn minimizes disruptions to end users
- Mean time to recovery (MTTR) - failures happen, but ability to quickly resolve and keep downtime at a minimum is a sign of a mature and well functioning organizations 
- Mean time between failures / Failure change rate - as teams move to faster deployment cycles, the initial failure rate can be quite high, but as DevOps practices mature, the deployment failure rate should dramatically improve over time
- Lead time - the time it takes to go from customer pain or idea to a feature successfully running in production
- Cycle time - the time it takes to go from code committed to code successfully running in production
- Efficiency - touch time and wait time as a percentage of cycle time and lead time
- Test coverage - continuous unit and functional testing is critical to deployment automation and speed
- Automated test pass % - since DevOps relies heavily on automated testing, it's important to monitor how well these test work

We will continue our journey by conducting research into how the usage of the different stages of GitLab correlates with positive results and attempt to quantify the relationships in a systematic, automated fashion. In the future, we hope we will be able to provide actionable recommendations to improve adoption and software development flows.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
We believe once developed, the score and related metrics would be of great interest to many Engineering and DevOps Managers, who are seeking to understand the adoption of the tool in more detail and how to better utilize it.

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
[Exploring a new DevOps Score](https://gitlab.com/gitlab-org/gitlab-ce/issues/41780)

<!--
### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
This category is currently at the `Minimal` maturity level and is planned to stay so for the rest of 2019. Please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend) and related [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=group%3A%3Aanalytics&label_name[]=devops%3A%3Amanage&label_name[]=Category%3ADevOps%20Score).

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

Epic for a [Minimal Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1479)
Epic for a [Viable Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1480)

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

<!--
### Competitive Landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!--
### Analyst Landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!--
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!--
### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!--
### Top internal customer issue(s)
These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

<!--
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
